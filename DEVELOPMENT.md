# Development

This documentation is for agalma developers.

## Git branching model

We use a simplified version of the branching model described at http://nvie.com/posts/a-successful-git-branching-model.

- `master`. stable releases. Each release is tagged. Merges into `master` only come from `devel` and `hotfix` branches.
- `devel`. unreleased complete and tested features.
- Unless otherwise specified, only Mark Howison can modify and merge into `master` and `devel`. There are a couple of exceptions when analysis code is not modified in any way. These include changes to documentation, which can be made directly in `devel` and, more rarely, in `master`. New tests should be added in feature branches, and if they all pass initially (i.e., no changes are needed to the base code) the author can merge them directly into `devel`.
- feature branches. These should be named after the corresponding issue number, eg `issue-47`. Feature branches always branch from `devel` and are merged into `devel`. They can only be merged into `devel` when the feature is complete and ready for release, the branch has successfully passed the `agalma test` regression test, the branch has passed all unit tests with `python setup.py test`, and the code has been reviewed. The typical workflow is that once the tests are passing, the author of the feature branch notifies Mark Howison that it is ready for code review and merging. They should be merged into `devel` with `--squash`, eg `git merge --squash issue-58`, so that a single, new commit object is always created. Do not delete the feature branch, and be sure to push it to the bitbucket repository, since that will retain all the individual commits from the feature's development.
- `hotfix`. Hot fixes can be applied to `master` in limited circumstances. If these fixes are on code rather than documentation, they should be made on the `hotfix` branch and tested with `agalma test` and unit tests with `python setup.py test` prior to being merged into `master`. After the hot fix has been applied to master, master should be merged into `devel` so that the changes are applied to the development branch as well.
- release branches. These should be named after the release number, eg `release-0.5.0` and are branched from `devel` once all feature branches going into the release are finished and merged into `devel`. The release branch is a place to commit all the small edits that happen while testing and preparing the release (eg bumping the version, testing out pip install, updating the install instructions and README, etc.). One the release is fully tested, the release branch is squash merged into `devel` with a message like "finished testing release 0.5.0", and then `devel` is squash merged into `master` with the release number as the message, eg "0.5.0".

Pull requests must pass `agalma test` prior to being pulled. Pull requests should be applied to their own branches that are branched off `devel`. The regression test should then be reapplied, and if it passes then the branch is merged back into `devel` with `--squash` (as for feature branches).

Commit messages should include references to issues so that it is clear which issues have been addressed or are in the process of being addressed. More on Issue Services at https://confluence.atlassian.com/display/BITBUCKET/Setting+Up+the+Issues+Service.

### Typical use case

The following is a typical use case for a new branch:

    git clone https://caseywdunn@bitbucket.org/caseywdunn/agalma.git
    cd agalma
    git checkout devel
    git pull
    git checkout -b issue-49
    # Make some edits
    git commit -am "added argument re #49"
    # Push the issue branch
    git push -u origin issue-49
    python setup.py test # Only proceed with the next steps if the test passes
    agalma test # Only proceed with the next steps if the test passes
    git checkout devel
    git merge --squash issue-49
    # The squash leaves the modified files in the working copy, and they need to be committed
    git commit -am "Fixes #N: description..."
    # Push the merged devel branch
    git push origin devel

The following is a typical use case for a new release:

    git checkout devel
    git merge master # fix any conflicts
    git checkout -b release-0.5.0
    # make all commits resulting from testing the release
    git checkout devel
    git merge --squash release-0.5.0
    git commit -am "finished testing release 0.5.0"
    git checkout master
    git merge --squash devel
    git commit -am "0.5.0"
    # sync up all the branches with the new master commit
    git checkout hotfix
    git merge master
    git checkout devel
    git merge master

### BioLite branching model

BioLite uses the same branching model as agalma. If a change to BioLite corresponds to modifications of agalma, the BioLite feature branch should be named accordingly, eg `agalma-issue-N`



## Release instructions

This section describes the process for releasing a new version of Agalma.

### Preparing the repository

See the sections above to understand the git branching model, which will result in changes being on particular branches pre-release. These instructions assume that this model has been followed.

First, make sure that the relevant changes are in the `devel` branch:

    git checkout devel
    git merge master # fix any conflicts
    git checkout -b release-0.5.0

Where `0.5.0` here and below is replaced with the release number.

Run the full (Tutorial)[https://bitbucket.org/caseywdunn/agalma/src/master/TUTORIAL.md] analysis on the `devel` branch with `agama test`. This is a required test of the code before making a release. Only proceed if it completes without error. If there are errors, fix and retest.

Update the NEWS file on the release branch to summarize the commits since the next release.

Update `__version__ =` in `agalma/__init__.py`.

### Releasing on Conda

Once the release branch is finalized, edit the recipe in dev/conda/agalma/meta.yaml to bump the versions in these sections:

    package:
      name: agalma
      version: 1.0.0

    source:
      fn: agalma-1.0.0.zip
      url: https://bitbucket.org/caseywdunn/agalma/get/release-1.0.0.zip
      sha256: 519f139e741d2f685f87c32dc8f198346b3017fab19682fd69ca082b290a848d

After you update the version numbers commit and push. The zip file in the url is automatically generated by Bitbucket from the latest commit on the release branch.  To update the sha256 sum, you will need to separately download the zip file and run sha256sum on it, e.g.:

    wget https://bitbucket.org/caseywdunn/agalma/get/release-1.0.0.zip
    sha256sum release-1.0.0.zip

Update the sha256 sum in the file, but do not commit and push yet.


Then build the image from inside the Docker build container with:

    docker run -it -v $HOME/agalma/dev/conda:/recipes mhowison/conda-build bash # change -v path to host agama repo
    conda update -n root conda-build # make sure you have the latest conda and conda build
    cd /recipes/<package-name>
    conda build -c dunnlab .

If the test passes and the recipe builds successfully, upload/release the package on anaconda.org from within the Docker container with:

    anaconda login
    anaconda upload <path to built .tar.bz2>

where the login username is "dunnlab".

Finally, commit and push the release branch.

This is the minimal set of instructions for generating a new Conda release and assumes that biolite has not changed. In particular, if the new agalma release has a corresponding release with biolite, or involves updating third-party dependencies, those recipes need to be updated and rebuilt first. The final recipe to be rebuilt will always be agalma.

More background on how the Conda recipes work: https://bitbucket.org/caseywdunn/agalma/wiki/Conda%20recipes

### Releasing on Docker Hub

https://bitbucket.org/caseywdunn/agalma/src/master/dev/docker/ has a Dockerfile to build a centos with Agalma installed via conda.

To build the image, run:

    cd dev/docker
    docker build .

To release the image on Dockerhub, run:

    docker tag <insert hash for new image> dunnlab/agalma:<version>
    docker tag <insert hash for new image> dunnlab/agalma:latest
    export DOCKER_ID_USER="dunnlab"
    docker login
    docker push dunnlab/agalma

### Syncing master

Once the release is finished and pushed to Anaconda and Dockerhub, finish syncing up the branches with:

    git checkout devel
    git merge --squash release-0.5.0
    git commit -am "finished testing release 0.5.0"
    git checkout master
    git merge --squash devel
    git commit -am "0.5.0"
    ## sync up all the branches with the new master commit
    git checkout hotfix
    git merge master
    git checkout devel
    git merge master
    git push origin --all
