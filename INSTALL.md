# Installation

These are advanced instructions for installing a development version of Agalma
from the git repository. See the Quick Install section of the README for
instructions on how to install a release version using Docker or Anaconda
Python.

See [TUTORIAL](https://bitbucket.org/caseywdunn/agalma/src/master/TUTORIAL.md)
for an example of how to use Agalma with a sample dataset.

## Reference list of dependencies

See the [conda recipe](dev/conda/agalma/meta.yaml) for a full list of
dependencies and their versions. All dependencies are available via
[bioconda](http://bioconda.github.io).

These dependencies include python modules and a variety of third-party tools
called from Agalma, which need to be in your PATH.

## Installing dependencies for development

Installing a release version of Agalma using Anaconda Python will automatically
install all the needed dependencies listed above. For a development version,
you can simply install the latest Agalma release then remove it (leaving all
of the dependencies still installed):

    conda create -c dunnlab -n agalma-dev agalma
    conda remove -n agalma-dev agalma

Now you can activate the "agalma-dev" environment, which will contain all of
the dependencies:

    source activate agalma-dev

## Installing Agalma form the git repo

For development work, or to access more recent features that are available
in the development branch, you can install Agalma directly from the git
repository:

    git clone https://bitbucket.org/caseywdunn/agalma.git
    cd agalma
    python setup.py install

If you edit the code in the repository, or pull a new version, then run the
following to update the installation:

    python setup.py install

You can use the development version of Agalma by checking out the devel branch
and reinstalling:

    git checkout devel
    python setup.py install

## Updating the BLAST databases

This step is optional and does not have to be done at every release.

First, regenerate the BLAST databases that are packaged with Agalma using the
scripts in the `dev` subdirectory:

    mkdir -p agalma/blastdb
    cd dev
    bash build-nt-rrna.sh
    bash build-swissprot.sh
    bash build-univec.sh

This will download a subset of the current GenBank (nt) database with only
titles containing "rrna", the current release of SwissProt, and the current
release of UniVec. The SwissProt database is parsed into a FASTA file that
includes the OG (Organelle) field in the description line.

The databases are packaged in their own download with:

    cd agalma
    zip -r ../agalma-blastdb-YYYYMMDD.zip blastdb
    cd ..

where YYYYMMDD is the date. This file is uploaded to the downloads section
of Bitbucket, and a corresponding entry in setup.py needs to be updated
to reflect the new filename, hash sum, and file size, e.g.:

    blastdb = {
      "url": "https://bitbucket.org/caseywdunn/agalma/downloads/agalma-blastdb-20170131.zip",
      "md5": "ffc28feae46af77d88766ef5cc0b31f5",
      "size": 150,
      "subdir": "blastdb"}

A similar mechanism is used to package the test data.

## Generating a tarball from the git repo

Then create a source distribution for Agalma with `distutils`:

    python setup.py sdist
