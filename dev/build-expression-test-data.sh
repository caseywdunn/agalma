#!/bin/sh
#SBATCH -c 8
#SBATCH -t 60
#SBATCH --mem=20g

# prefetch and fastq-dump from sratoolkit version 2.5.4
prefetch SRR089297
fastq-dump SRR089297
prefetch SRR081276
fastq-dump SRR081276

# path to Nanomia bijuga assembled in expression test
ASSEMBLY=Trinity

# bowtie2 version 2.1.0
bowtie2-build ${ASSEMBLY}.fasta $ASSEMBLY
bowtie2 --very-fast-local -p 8 -x $ASSEMBLY -U SRR089297.fastq --al SRX036876.fq
bowtie2 --very-fast-local -p 8 -x $ASSEMBLY -U SRR081276.fastq --al SRX033366.fq

