#!/bin/bash
set -e
rm -f UniVec
wget http://ftp.ncbi.nih.gov/pub/UniVec/UniVec
makeblastdb -dbtype nucl -in UniVec -title "Agalma UniVec build $(date)" -out ../agalma/blastdb/UniVec
