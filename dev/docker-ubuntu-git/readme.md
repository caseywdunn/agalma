# docker-ubuntu-git

This Dockerfile is intented for Agalma development and testing. It is not officially supported. 

It it is based on the "Installing Agalma form the git repo" instructions at (INSTALL)[https://bitbucket.org/caseywdunn/agalma/src/master/INSTALL.md].

To build a Docker container with Agalma from a specific branch of this repository, execute:

    docker build --build-arg agalma_branch=master https://bitbucket.org/caseywdunn/agalma/raw/master/dev/docker-ubuntu-git/Dockerfile

Where `agalma_branch=` specifies the branch fromt he agalma repository that you want to install. Note that this only impacts the version 
of Agalma that is installed, not its dependencies.

If all goes well this will finish with the line `Successfully built [id]`, where `[id]` is the image ID for the image you just built. 
You can then run a container based on this id with (substituting `[id]`):

    docker run -it [id] bash

See the "Installing Agalma form the git repo" instructions at (INSTALL)[https://bitbucket.org/caseywdunn/agalma/src/master/INSTALL.md] for additional instructions on testing, pulling changes, etc.